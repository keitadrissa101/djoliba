/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author Administrateur
 */
public class Classes {
    private int id;
    private String nom;
    private int inscription;
    private int annee;

    public Classes() {
    }

    public Classes(String nom, int inscription, int annee) {
        this.nom = nom;
        this.inscription = inscription;
        this.annee = annee;
    }

    public Classes(int id, String nom, int inscription, int annee) {
        this.id = id;
        this.nom = nom;
        this.inscription = inscription;
        this.annee = annee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getInscription() {
        return inscription;
    }

    public void setInscription(int inscription) {
        this.inscription = inscription;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }
    
    
    
}
