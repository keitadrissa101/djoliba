/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Classe;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hp
 */
public class ClasseDao extends Dao<Classe> {

    @Override
    public Classe rechercher(long id) {
        ResultSet result = null;
        Classe c = new Classe();
        try {
           result = this.connect.createStatement().executeQuery("SELECT * FROM classes WHERE id = "+id);
           if(result.next()){
               c.setId(result.getInt("id"));
               c.setNom(result.getString("nom"));
               c.setInscription(result.getInt("inscription"));
               c.setAnnee(result.getInt("annee"));
           }
        } catch (SQLException ex) {
            Logger.getLogger(ClasseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
       return c;
    }

    @Override
    public void inserer(Classe c) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO classes (id,nom,inscription,annee) VALUES (?,?,?,?)");
            prepare.setInt(1, c.getId());
            prepare.setString(2, c.getNom());
            prepare.setInt(3, c.getInscription());
            prepare.setInt(4, c.getAnnee());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ClasseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Classe e, long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE classes SET id = ?,nom =?,inscription=?, annee = ? WHERE id ="+id);
            prepare.setInt(1, e.getId());
            prepare.setString(2,e.getNom() );
            prepare.setInt(3,e.getInscription());
            prepare.setInt(4,e.getAnnee());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ClasseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(long id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM classes WHERE id ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ClasseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Classe> liste() {
        List<Classe> cl = new ArrayList();
        Classe obj = new Classe();
        ResultSet result;
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM classes");
            while(result.next()){
            obj.setId(result.getInt("id"));
            obj.setNom(result.getString("nom"));
            obj.setInscription(result.getInt("inscription"));
            obj.setAnnee(result.getInt("annee"));
            cl.add(obj);
            obj = new Classe();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(EtudiantDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cl;
    }
    
}
